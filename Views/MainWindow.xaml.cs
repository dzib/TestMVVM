﻿using System.Windows;
using Test.ViewModels;

namespace Test.Views
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();           
            DataContext = new MainWindowViewModel();
        }
    }
}
