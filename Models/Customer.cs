﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.ComponentModel;
using System.Windows.Input;

namespace Test.Models
{
    public partial class Customer : ObservableObject
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public ICommand? AddToList { get; }

        public Customer(string lastName, string firstName)
        {
            LastName = lastName;
            FirstName = firstName;
            AddToList = null;
        }
    }
}
