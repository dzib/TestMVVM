﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System.Collections.ObjectModel;
using Test.Models;

namespace Test.ViewModels
{
    public partial class MainWindowViewModel : ObservableObject
    {
        [ObservableProperty]
        [NotifyPropertyChangedFor(nameof(FullName))]
        [NotifyCanExecuteChangedFor(nameof(ClickCommand))]
        private string? _lastName;

        [ObservableProperty]
        [NotifyPropertyChangedFor(nameof(FullName))]
        [NotifyCanExecuteChangedFor(nameof(ClickCommand))]
        private string? _firstName;

        private ObservableCollection<Customer> Customers = new();

        public string FullName => $"{FirstName} {LastName}";
        private bool CanClick() => FirstName != "exit";

        [RelayCommand(CanExecute = nameof(CanClick))]
        private void Click()
        {
            if (LastName != null && FirstName != null)
                Customers.Add(new Customer(LastName, FirstName));
        }
    }
}
